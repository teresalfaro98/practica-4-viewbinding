package com.example.practice4_helloword.fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.practice4_helloword.R
import com.example.practice4_helloword.databinding.FragmentMainBinding

class MainFragment : Fragment(R.layout.fragment_main) {


    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding !!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater,container,false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var nombre ="Emmanuel"

        binding.btnFragmentDetail.setOnClickListener {
            /*findNavController().navigate(R.id.action_mainFragment_to_detailFragment, bundleOf(
                "nombre" to nombre
            ))

            val action = MainFragmentDirections.actionMainFragmentToDetailFragment()
            action.nombre = nombre

            findNavController().navigate(action)*/
            //findNavController().navigate(R.id.action_mainFragment_to_main2_graph)

            findNavController().navigate(Uri.parse("myapp://blan2Fragment"))
        }
    }

}