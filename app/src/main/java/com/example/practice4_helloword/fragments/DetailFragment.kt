package com.example.practice4_helloword.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.example.practice4_helloword.R
import com.example.practice4_helloword.databinding.FragmentDetailBinding

class DetailFragment : Fragment(R.layout.fragment_detail) {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding !!

    private var nombre:String? = ""
    val args:DetailFragmentArgs by navArgs() //Delegation

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /*arguments?.let { bundle ->
            nombre = bundle.getString("nombre")
        }*/
        nombre = args.nombre
        Toast.makeText(this.activity,nombre,Toast.LENGTH_LONG).show()
    }

}