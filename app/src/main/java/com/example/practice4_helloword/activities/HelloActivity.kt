package com.example.practice4_helloword.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.practice4_helloword.R
import com.example.practice4_helloword.databinding.ActivityHelloBinding

class HelloActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHelloBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityHelloBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val name = intent.getStringExtra("name")
        val hello = getString(R.string.hello)

        binding.txtName.text = "$hello $name"

        binding.btnBack.setOnClickListener{
            finish()
        }

        binding.btnBackReturn.setOnClickListener{
            var intent = Intent()
            intent.putExtra("valor1","Test1")
            setResult(Activity.RESULT_OK,intent)
            finish()
        }
    }
}