package com.example.practice4_helloword.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.practice4_helloword.R
import com.example.practice4_helloword.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val TAG = "TEST"
    private val TAG2 = "CICLO"

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSayHello.setOnClickListener{
            val name = binding.editName.text.toString()
            sayHello(name = name)
        }

        binding.btnSayHello2.setOnClickListener{
            var name = binding.editName.text.toString()
            var intent = Intent(this,HelloActivity::class.java)
            intent.putExtra("name",name)
            //startActivity(intent)

            startActivityForResult(intent,1)
        }

        binding.btnToolbar.setOnClickListener{
            var intent = Intent(this,ToolbarActivity::class.java)
            startActivity(intent)
        }

        binding.btnFragments.setOnClickListener{
            var intent = Intent(this,FragmentActivity::class.java)
            startActivity(intent)
        }

        binding.btnFragments2.setOnClickListener{
            var intent = Intent(this,Fragment2Activity::class.java)
            startActivity(intent)
        }



        //Log.e(TAG,"This is an error")
        Log.d(TAG,"This is for debuging")
        Log.w(TAG,"This is a warning")
        Log.i(TAG,"This is informative")

    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG2,"onStart")
    }

    override fun onResume(){
        super.onResume()
        Log.d(TAG2, "onResume")
    }



    override fun onPause(){
        super.onPause()
        Log.d(TAG2, "onPause")
    }



    override fun onStop(){
        super.onStop()
        Log.d(TAG2, "onStop")
    }



    override fun onRestart(){
        super.onRestart()
        Log.d(TAG2, "onRestart")
    }



    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG2, "onDestroy")
    }

    fun sayHello(name:String){
        var hello = getString(R.string.hello)
        binding.txtLabel.text = "$hello $name"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            val valor1 = data?.getStringExtra("valor1")
            Toast.makeText(this,"$valor1",Toast.LENGTH_LONG).show()
        }
    }
}