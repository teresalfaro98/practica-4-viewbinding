package com.example.practice4_helloword.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practice4_helloword.R
import com.example.practice4_helloword.databinding.ActivityFragment2Binding

class Fragment2Activity : AppCompatActivity() {

    private lateinit var binding: ActivityFragment2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFragment2Binding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}